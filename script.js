var searchModule = (function () {

    var _input,
        _output,
        _value;

    function _updateOutput(event) {
        _value = _input.value;
        console.log(_value);
        _output.innerHTML = _value;
    }

    function init(querySelectorInput, querySelectorOutput) {
        _input = document.querySelector(querySelectorInput);
        _output= document.querySelector(querySelectorOutput);
        console.log('running init', _input, _output);
        _input.addEventListener('keyup', _updateOutput);
    }

    return {
        init: init
    }


})();

searchModule.init('.input', '.message');




// function Search(querySelectorInput, querySelectorOutput) {
//
//     var input,
//         output,
//         value;
//
//     function updateOutput(event) {
//         value = input.value;
//         console.log(value);
//         output.innerHTML = value;
//     }

//     (function init() {
//         input = document.querySelector(querySelectorInput);
//         _output= document.querySelector(querySelectorOutput);
//         console.log('running init', input, output);
//         input.addEventListener('keyup', updateOutput);
//     })();
//
// }
//
// new Search('.input', '.message');
//
